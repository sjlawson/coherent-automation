---
layout: page
title: Privacy and Terms of Use
permalink: /terms/
---

<div>
  <p>Please refer to the following documents:</p>
  <p>
    <a href="privacy.pdf" target="_blank" >Privacy statement</a>
  </p>
  <p>
    <a href="tou.pdf" target="_blank" >Website & application terms of use</a>
  </p>
</div>
