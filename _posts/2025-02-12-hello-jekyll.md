---
layout: post
title:  "The right tools"
date:   2025-02-12 11:00:00 -0500
categories: jekyll update
---

## Right tool, right job

### This site is built with Jekyll
This site was build with Jekyll, a static website compositor written in the Ruby programming language.  

In my 20+ years of software engineering and web development, there's one problem that stands out to me that nearly every business suffers from: Over-engineering  

I would absolutely use Django for a multi-tiered web application, FastAPI to make a simple API layer, or Flask for a data application dedicated to a specific task.  
I would *not* use those for a simple web site - not when something exists that can do the job well without the bloat for absolutely trivial hosting cost. 

Those frameworks and content management systems (CMS) are great for building enterprise web applications. I happen to think they're terrible for the average website - even for a business.  
  
If a site is meant to be a landing page or a simple information tool, using anything more than HTML/CSS and a bit of Javascript is overkill.  

For something slightly more complex, static site builders like [Jekyll](https:jekyllrb.com) are ideal. You still separate site layout and functionality from content, but without all the excess overhead of a CMS. 

Jekyll is just one such site builder. I like it for its ease of use and ability to extend even into some very complex website designs that use modern Javascript frameworks like NextJs and Vue. Here's another Jekyll site that showcases a certain musician who works in a variety of styles: [Azure Obscura][https://azulobscura.com]  


##  a Basic Jekyll site directory structure

```
.
├── 404.html
├── _config.yml
├── index.html
├── about.md
├── css
│   └── main.scss
├── images
│   └── logo.png
├── _includes
│   ├── footer.html
│   ├── header.html
│   └── head.html
├── _layouts
│   ├── default.html
│   ├── empty.html
│   ├── page.html
│   └── post.html
├── _posts
│   └── 2024-03-24-welcome-to-jekyll.md
├── _sass
│   ├── _base.scss
│   ├── _layout.scss
│   └── _syntax-highlighting.scss
├── README.md
├── feed.xml
├── Gemfile
└── Gemfile.lock

```
... and you can even get by on less. 

Essentially, you set your site name and other meta data in _config, the overall layout in `_layouts` and then your pages are either HTML or markdown. As you can see, the content is kepy neatly separated from meta data and layout/styles -- essentially solving the problem that content management systems set-out to solve, but mostly just made more complex.  

Sure you still need a CMS for some scenarios, and you absolutely need database-driven sites if you want to manage users, but for the average "Brochure" webpage, *if* you need more than a simple `index.html` landing page, Jekyll hydes complexity from everyday website management so you can just make content. (Yeah, totally intented that pun)


## Here's the original post about how to make posts with Jekyll

To add new posts, simply add a file in the `_posts` directory that follows the convention `YYYY-MM-DD-name-of-post.ext` and includes the necessary front matter. Take a look at the source for this post to get an idea about how it works.

Jekyll also offers powerful support for code snippets:

```python
def print_hi(name):
    print(f"Hi, {name}")

print_hi('Tom')
# prints 'Hi, Tom' to STDOUT.
```

Check out the [Jekyll docs][jekyll-docs] for more info on how to get the most out of Jekyll. File all bugs/feature requests at [Jekyll’s GitHub repo][jekyll-gh]. If you have questions, you can ask them on [Jekyll Talk][jekyll-talk].

[jekyll-docs]: http://jekyllrb.com/docs/home
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/
